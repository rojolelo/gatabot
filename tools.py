# helper functions used by the bot
from discord import ChannelType
import os
from random import randint
import datetime

# Returns the name of a random file in the directory specified
def random_file(directory):

    files = os.listdir(directory)

    # Now we pick a random number limited by the amount of files

    return files[randint(0, len(files) - 1)]


# Returns true if the date is within a month in the past/future of the current date.
# Returns false otherwise
def valid_date(dateToCheck):

    today = datetime.date.today()

    dateToCheck = datetime.date(
        int(dateToCheck[-2:]) + 2000, int(dateToCheck[:2]), int(dateToCheck[3:-3])
    )

    difference = today - dateToCheck

    if int(difference.days) > 30:
        return False

    else:
        return True


# Checks that all entries in chimbas.txt are less than a month
# old, updates the file with only valid entries and returns a list
# of valid names contained in the database
def update_database():

    # First read the file and only obtain valid entries
    file = open("chimbas.txt", "r")
    dates = []
    names = []
    for line in file:

        gata = line.rstrip("\n")
        date = gata[-8:]
        if valid_date(date):

            names.append(gata[:-8].rstrip(" "))
            dates.append(date)

    file.close()

    # erase the file and write only valid entries
    file = open("chimbas.txt", "w")

    for i in range(len(names)):
        file.write(names[i] + " " + dates[i] + "\n")

    file.close()

    return names


# returns the voice channel the user is connected to
# returns None if user is not in any voice channel
def find_voice_channel(user, client):

    for channel in client.get_all_channels():
        if channel.type == ChannelType.voice:

            for member in channel.members:

                if member == user:
                    return channel

    return None


def check_birthdays():
    today = datetime.date.today()
    birthday_gatas = []

    data = open("birthdays.csv", "r")

    for line in data:
        name, birthday = line.split(",")
        month, day, year = birthday.split("/")

        if today.month == month and today.day == day:
            birthday_gatas.append(name)

    data.close()

    return birthday_gatas


def get_channel(bot, name):

    channels = bot.guilds[0].text_channels

    for channel in channels:

        if name in channel.name:
            return channel
